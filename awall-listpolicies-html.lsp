<% local form, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
        $(document).ready(function() {
		$("#list").tablesorter({widgets: ['zebra']});
		$(".deletepolicy").click(function(){ return confirm("Are you sure you want to delete this policy?")});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"deletepolicy", "editpolicy", "viewpolicy", "enablepolicy", "disablepolicy", "startstop"}, session) %>
<% htmlviewfunctions.displaycommandresults({"createpolicy"}, session, true) %>

<% local header_level = htmlviewfunctions.displaysectionstart(form, page_info) %>
<% htmlviewfunctions.displayinfo(form) %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Status</th>
		<th>Name</th>
		<th>Description</th>
	</tr>
</thead><tbody>
<% local filename = cfe({ type="hidden", value="" }) %>
<% local name = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,pol in ipairs(form.value) do %>
	<tr>
		<td>
			<%
			filename.value = pol.filename
			name.value = pol.name
			if viewlibrary.check_permission("deletepolicy") and pol.editable and pol.status ~= "enabled" and pol.status ~= "required" then
				htmlviewfunctions.displayitem(cfe({type="form", value={filename=filename}, label="", option="Delete", action="deletepolicy", class="deletepolicy"}), page_info, -1)
			end
			if viewlibrary.check_permission("editpolicy") and pol.editable then
				htmlviewfunctions.displayitem(cfe({type="link", value={filename=filename, redir=redir}, label="", option="Edit", action="editpolicy"}), page_info, -1)
			end
			if viewlibrary.check_permission("viewpolicy") and not pol.editable and pol.filename then
				htmlviewfunctions.displayitem(cfe({type="link", value={filename=filename, redir=redir}, label="", option="View", action="viewpolicy"}), page_info, -1)
			end
			if viewlibrary.check_permission("enablepolicy") and (pol.status == "disabled" or pol.status == "required") then
				htmlviewfunctions.displayitem(cfe({type="form", value={name=name}, label="", option="Enable", action="enablepolicy"}), page_info, -1)
			end
			if viewlibrary.check_permission("disablepolicy") and pol.status == "enabled" then
				htmlviewfunctions.displayitem(cfe({type="form", value={name=name}, label="", option="Disable", action="disablepolicy"}), page_info, -1)
			end
			%>
		</td>
		<td><%= html.html_escape(string.gsub(pol.status or "", "^%l", string.upper)) %></td>
		<td><%= html.html_escape(pol.name) %></td>
		<td><%= html.html_escape(pol.description) %></td>
	</tr>
<% end %>
</tbody></table>
<% htmlviewfunctions.displaysectionend(header_level) %>

<% if viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("createpolicy") then
	viewlibrary.dispatch_component("createpolicy")
end %>

<% if viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("startstop") then
	viewlibrary.dispatch_component("startstop")
end %>
