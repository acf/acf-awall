local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.startstop(self)
        return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.listpolicies(self)
	return self.model.list_policies()
end

function mymodule.viewpolicy(self)
	return self.model.read_policyfile(self, self.clientdata)
end

function mymodule.createpolicy(self)
	return self.handle_form(self, self.model.get_newpolicy, self.model.create_policy, self.clientdata, "Create", "Create New Policy File", "Policy File Created")
end

function mymodule.deletepolicy(self)
	return self.handle_form(self, self.model.get_delete_policy, self.model.delete_policy, self.clientdata, "Delete", "Delete Policy File", "Policy File Deleted")
end

function mymodule.editpolicy(self)
	return self.handle_form(self, self.model.get_policyfile, self.model.update_policyfile, self.clientdata, "Save", "Edit Policy", "Policy File Saved")
end

function mymodule.enablepolicy(self)
	return self.handle_form(self, self.model.get_enablepolicy, self.model.enable_policy, self.clientdata, "Enable", "Enable Policy", "Policy Enabled")
end

function mymodule.disablepolicy(self)
	return self.handle_form(self, self.model.get_enablepolicy, self.model.disable_policy, self.clientdata, "Disable", "Disable Policy", "Policy Disabled")
end

return mymodule
